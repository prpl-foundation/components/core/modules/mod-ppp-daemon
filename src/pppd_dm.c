/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stddef.h>

#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>

#include <pppd/pppd.h>
#include <pppd/fsm.h>
#include <pppd/ipcp.h>
#include <pppd/eui64.h>
#include <pppd/ipv6cp.h>

#include "ppp.h"
#include "pppd_dm.h"
#include "pppd_utils.h"

#define PPP_LAST_CONN_ERROR_ISP_TIME_OUT              "ERROR_ISP_TIME_OUT"
#define PPP_LAST_CONN_ERROR_COMMAND_ABORTED           "ERROR_COMMAND_ABORTED"
#define PPP_LAST_CONN_ERROR_NOT_ENABLED_FOR_INTERNET  "ERROR_NOT_ENABLED_FOR_INTERNET"
#define PPP_LAST_CONN_ERROR_BAD_PHONE_NUMBER          "ERROR_BAD_PHONE_NUMBER"
#define PPP_LAST_CONN_ERROR_USER_DISCONNECT           "ERROR_USER_DISCONNECT"
#define PPP_LAST_CONN_ERROR_ISP_DISCONNECT            "ERROR_ISP_DISCONNECT"
#define PPP_LAST_CONN_ERROR_IDLE_DISCONNECT           "ERROR_IDLE_DISCONNECT"
#define PPP_LAST_CONN_ERROR_FORCED_DISCONNECT         "ERROR_FORCED_DISCONNECT"
#define PPP_LAST_CONN_ERROR_SERVER_OUT_OF_RESOURCES   "ERROR_SERVER_OUT_OF_RESOURCES"
#define PPP_LAST_CONN_ERROR_RESTRICTED_LOGON_HOURS    "ERROR_RESTRICTED_LOGON_HOURS"
#define PPP_LAST_CONN_ERROR_ACCOUNT_DISABLED          "ERROR_ACCOUNT_DISABLED"
#define PPP_LAST_CONN_ERROR_ACCOUNT_EXPIRED           "ERROR_ACCOUNT_EXPIRED"
#define PPP_LAST_CONN_ERROR_PASSWORD_EXPIRED          "ERROR_PASSWORD_EXPIRED"
#define PPP_LAST_CONN_ERROR_AUTHENTICATION_FAILURE    "ERROR_AUTHENTICATION_FAILURE"
#define PPP_LAST_CONN_ERROR_NO_DIALTONE               "ERROR_NO_DIALTONE"
#define PPP_LAST_CONN_ERROR_NO_CARRIER                "ERROR_NO_CARRIER"
#define PPP_LAST_CONN_ERROR_NO_ANSWER                 "ERROR_NO_ANSWER"
#define PPP_LAST_CONN_ERROR_LINE_BUSY                 "ERROR_LINE_BUSY"
#define PPP_LAST_CONN_ERROR_UNSUPPORTED_BITSPERSECOND "ERROR_UNSUPPORTED_BITSPERSECOND"
#define PPP_LAST_CONN_ERROR_TOO_MANY_LINE_ERRORS      "ERROR_TOO_MANY_LINE_ERRORS"
#define PPP_LAST_CONN_ERROR_IP_CONFIGURATION          "ERROR_IP_CONFIGURATION"
#define PPP_LAST_CONN_ERROR_UNKNOWN                   "ERROR_UNKNOWN"

char* nic_name = NULL;       // e.g: eth0
char* interface_name = NULL; // As defined in PPP.Interface.Name (e.g: "pppoe-wan");

/*
 * Update the datamodel.
 */
static void execute_update(const amxc_var_t* extra_data) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&ret, AMXC_VAR_ID_HTABLE);

    amxc_var_copy(&data, extra_data);
    amxc_var_add_key(cstring_t, &data, "ifname", interface_name);
    amxc_var_add_key(cstring_t, &data, "device", nic_name);

    retval = amxm_execute_function("self", MOD_CORE, "update-dm", &data, &ret);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Mod %s, func 'update-dm' returned %d", MOD_CORE, retval);
    }

    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

/*
 * Helper function that is used to change one single string value in the datamodel.
 * For example: set_key_value("AuthenticationProtocol", "CHAP");
 */
void set_key_value(const char* key, const char* value) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &data, key, value);

    execute_update(&data);

    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

/*
 * Helper function that is used to change one single integer value in the datamodel.
 * For example: set_key_value("SessionID", "42");
 */
void set_key_value_int(const char* key, const int value) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(uint32_t, &data, key, value);

    execute_update(&data);

    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

/*
 * When the pppd process is either:
 *  - started succesfully and the process has assigned an ip address
 *  - stopped which means that the connection is down
 * this function is called to update the datamodel.
 * It effectively replaces the ppp-up and ppp-down scripts that are used in mod-ppp-direct and mod-ppp-uci.
 */
void set_ipcp(bool enabled) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "IPv4 is %s!", (enabled ? "up" : "down"));

    amxc_var_add_key(bool, &data, "enabled", enabled);
    amxc_var_add_key(bool, &data, "update_status", false);
    if(enabled) {
        ipcp_options* ho = &ipcp_hisoptions[0];
        ipcp_options* go = &ipcp_gotoptions[0];

        amxc_var_t dns_list;
        amxc_var_init(&dns_list);
        amxc_var_set_type(&dns_list, AMXC_VAR_ID_LIST);

        if(go != NULL) {
            amxc_string_t dns_string;
            amxc_string_init(&dns_string, 0);

            amxc_var_add(cstring_t, &dns_list, ip_ntoa(go->dnsaddr[0]));
            amxc_var_add(cstring_t, &dns_list, ip_ntoa(go->dnsaddr[1]));
            amxc_string_csv_join_var(&dns_string, &dns_list);

            amxc_var_add_key(cstring_t, &data, "DNSServers", amxc_string_get(&dns_string, 0));
            amxc_var_add_key(cstring_t, &data, "LocalIPAddress", ip_ntoa(go->ouraddr));

            amxc_string_clean(&dns_string);
        }
        if(ho != NULL) {
            amxc_var_add_key(cstring_t, &data, "RemoteIPAddress", ip_ntoa(ho->hisaddr));
        }

        amxc_var_clean(&dns_list);
    }

    execute_update(&data);

    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

/*
 * Make a string representation of a network address.
 * Copied over from pppd codebase.
 */
static char* llv6_ntoa(eui64_t ifaceid) {
    SAH_TRACEZ_IN(ME);
    static char b[64];

    sprintf(b, "fe80::%s", eui64_ntoa(ifaceid));
    SAH_TRACEZ_OUT(ME);
    return b;
}

void set_ipcpv6(bool enabled) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "IPv6 is %s!", (enabled ? "up" : "down"));

    amxc_var_add_key(bool, &data, "enabled_v6", enabled);
    amxc_var_add_key(bool, &data, "update_status", false);
    if(enabled) {
        ipv6cp_options* ho = &ipv6cp_hisoptions[0];
        ipv6cp_options* go = &ipv6cp_gotoptions[0];

        if(go != NULL) {
            amxc_var_add_key(cstring_t, &data, PPP_LOCAL_INTF_ID, llv6_ntoa(go->ourid));
        }
        if(ho != NULL) {
            amxc_var_add_key(cstring_t, &data, PPP_REMOTE_INTF_ID, llv6_ntoa(ho->hisid));
        }
    }

    execute_update(&data);

    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
}

void set_last_connection_error(int value) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_WARNING(ME, "PPPD process stopped with status: %s", status_to_string(value));

    switch(value) {
    case EXIT_OK:
        set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_NONE); break;
    case EXIT_USER_REQUEST:       // Stopped by user
        set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_USER_DISCONNECT); break;
    case EXIT_IDLE_TIMEOUT:
        set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_IDLE_DISCONNECT); break;
    case EXIT_PEER_DEAD:            // No response to LCP Echo Requests
        set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_NO_ANSWER); break;
    case EXIT_PEER_AUTH_FAILED:     // Auth Fail: Peer could not authenticate itself  falltrough
    case EXIT_AUTH_TOPEER_FAILED:   // Auth Fail: Failed to authenticate ourself
    case EXIT_PEER_AUTH_NO_REPLY:   // Auth Fail: No replies from peer while it tries to authenticate itself
    case EXIT_AUTH_TOPEER_NO_REPLY: // Auth Fail: No replies from peer while trying to authenticate ourself
        set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_AUTHENTICATION_FAILURE); break;
    case EXIT_CNID_AUTH_FAILED:
        set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_BAD_PHONE_NUMBER); break;
    case EXIT_FATAL_ERROR:
    case EXIT_OPTION_ERROR:
    case EXIT_NOT_ROOT:
    case EXIT_NO_KERNEL_SUPPORT:
    case EXIT_LOCK_FAILED:
    case EXIT_OPEN_FAILED:
    case EXIT_CONNECT_FAILED:     // Discovery failed
    case EXIT_PTYCMD_FAILED:
    case EXIT_NEGOTIATION_FAILED: // Discovery OK but negotiation failed
    case EXIT_CONNECT_TIME:
    case EXIT_CALLBACK:           // Used only in CBCP Protocol, Not Applicable atm
    case EXIT_HANGUP:
    case EXIT_LOOPBACK:
    case EXIT_INIT_FAILED:
    #ifdef MAXOCTETS
    case EXIT_TRAFFIC_LIMIT:
    #endif
    default:
        set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_UNKNOWN); break;
    }
    SAH_TRACEZ_OUT(ME);
}
