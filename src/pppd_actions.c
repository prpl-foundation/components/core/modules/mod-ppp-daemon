/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <fcntl.h>
#include <pwd.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <pppd/pppd.h>
#include <pppd/fsm.h>
#include <pppd/ccp.h>
#include <pppd/lcp.h>
#include <pppd/magic.h>
#include <pppd/pathnames.h>
#include <pppd/generator.h>

#include "ppp.h"
#include "pppd_actions.h"
#include "pppd_utils.h"
#include "pppd_signals.h"
#include "pppd_state.h"

#define PROGNAME "pppd"

// Restart information
amxp_timer_t* restart_timer = NULL;
static bool restart_auth_failed_schema = true;
static amxc_var_t* restart_args = NULL;
static uint32_t restart_delay = 0;
static char* lcp_echo_restart_delay = NULL;

/*
 * Store the pppd process options that are currently in use.
 * Used to check if we need to restart the process when new options are set.
 */
static amxc_var_t* current_options = NULL;
static timepoints_t* authentication_failed_schema = NULL;
static int discovery_fd = 0;


#ifdef UNIT_TEST
amxc_var_t* get_current_settings() {
    return current_options;
}

amxc_var_t* get_restart_settings() {
    return restart_args;
}
#endif

int* get_discovery_fd(void) {
    return &discovery_fd;
}

static void blocking_mainloop(void) {
    SAH_TRACEZ_IN(ME);
    when_false(BLOCKING, exit);

    while(current_phase != PHASE_DEAD) {
        pppd_handle_events();
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

/*
 * Initialize the pppd process with the options stored in the variant.
 */
static int start(amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    int rc = EXIT_OPTION_ERROR;
    int nr_listening_fds = 0;
    struct protent* protp = NULL;
    amxc_var_t fd_list;
    amxc_var_init(&fd_list);
    amxc_var_set_type(&fd_list, AMXC_VAR_ID_LIST);

    when_str_empty_trace(GET_CHAR(args, "ifname"), exit, ERROR, "ifname should be set");
    when_str_empty_trace(GET_CHAR(args, "name"), exit, ERROR, "name should be set");
    when_str_empty_trace(GET_CHAR(args, "auth_failed_interval"), exit, ERROR, "auth_failed_interval should be set");
    when_str_empty_trace(GET_CHAR(args, "lcp_echo_restart_delay"), exit, ERROR, "lcp_echo_restart_delay should be set");

    nr_listening_fds = get_nr_fds();
    when_false_trace(nr_listening_fds == 0, exit, ERROR, "expected zero listening file descriptors, found %d", nr_listening_fds);

    nic_name = strdup(GET_CHAR(args, "ifname"));
    interface_name = strdup(GET_CHAR(args, "name"));
    if(restart_auth_failed_schema) {
        if(authentication_failed_schema != NULL) {
            cleanup_timepoints(&authentication_failed_schema);
        }
        authentication_failed_schema = initialize_timepoints(GET_CHAR(args, "auth_failed_interval"));
        when_null_trace(authentication_failed_schema, exit, ERROR, "could not initialize authentication retry algorithm");
    }
    lcp_echo_restart_delay = strdup(GET_CHAR(args, "lcp_echo_restart_delay"));
    restart_delay = GET_UINT32(args, "restart_delay");
    devnam_fixed = 0;

    rc = init_pre_settings(protp);
    when_failed_trace(rc, exit, ERROR, "Failed to initialize pppd protocols");

    progname = (char*) PROGNAME;
    rc = pppd_set_options(args);
    when_failed_trace(rc, exit, ERROR, "Failed to load pppd settings");

    if(amxc_var_is_null(current_options)) {
        amxc_var_new(&current_options);
    }
    amxc_var_copy(current_options, args);

    amxc_var_delete(&restart_args);
    restart_args = NULL;

    rc = init_settings(protp, false);

    if(rc == -1) {  // dryrun
        rc = 0;
        goto exit;
    }
    when_failed_trace(rc, exit, ERROR, "Failed to initialize pppd settings");

    /*
     * Copied from pppd daemon codebase:
     * Apparently we can get a SIGPIPE when we call syslog, if
     * syslogd has died and been restarted.  Ignoring it seems
     * be sufficient.
     */
    signal(SIGPIPE, SIG_IGN);

    rc = init_connection(BLOCKING);
    when_failed_trace(rc, exit, ERROR, "Failed to start connection");

    /*
     * One of the last steps in the init phase is to discover a PPP server that we can connect to.
     * Because the original pppd codebase is using blocking calls we created our own discovery mechanism.
     * By adding the discovery file descriptor to the amx eventloop and only act when data is available we can make this process async.
     * When the discovery is completed succesfully a custom hook (discovery_succeeded) is triggered to finalize the discovery phase.
     */
    discovery_fd = start_link(0, BLOCKING);
    if(BLOCKING) {
        blocking_mainloop();
    } else {
        amxc_var_add(fd_t, &fd_list, discovery_fd);
        SAH_TRACEZ_INFO(ME, "Adding discovery_fd %d to amx eventloop", discovery_fd);
        add_fds(&fd_list, "handle-discovery");
        if(the_channel != NULL) {
            the_channel->discovery();
        }
    }

exit:
    amxc_var_clean(&fd_list);
    if(rc != 0) {
        free(nic_name);
        nic_name = NULL;

        free(interface_name);
        interface_name = NULL;

        free(lcp_echo_restart_delay);
        lcp_echo_restart_delay = NULL;
    }
    SAH_TRACEZ_OUT(ME);
    return rc;
}

/*
 * Stop the pppd process by sending a SIGINT signal to it.
 */
static int stop(void) {
    SAH_TRACEZ_IN(ME);
    int rc = amxp_sigmngr_emit_signal(NULL, strsignal(SIGINT), NULL);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

/*
 * Timer callback function used to restart the pppd process after it was closed previously by the process itself.
 * Possible reasons are: discovery failure, authentication failure, idle time reached,...
 */
void restart_timer_timeout(UNUSED amxp_timer_t* t, UNUSED void* data) {
    SAH_TRACEZ_IN(ME);
    int rc = -1;
    bool reset = GET_BOOL(restart_args, "reset");
    set_state(STATE_READY_FOR_RESTART);

    if(amxc_var_is_null(restart_args)) {
        amxc_var_new(&restart_args);
        amxc_var_copy(restart_args, current_options);
    }

    rc = manage_process(restart_args, reset);
    when_failed_trace(rc, exit, ERROR, "Failed to restart pppd process");

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void update_restart_args(amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    if(amxc_var_is_null(restart_args)) {
        amxc_var_new(&restart_args);
    } else {
        SAH_TRACEZ_WARNING(ME, "Resetting restart values because newer settings are present!");
    }

    amxc_var_copy(restart_args, args);
    SAH_TRACEZ_OUT(ME);
}

/*
 * Based on the given args the pppd process will be started/stopped/restarted/do nothing
 */
int manage_process(amxc_var_t* args, bool reset) {
    SAH_TRACEZ_IN(ME);
    int res = 0;
    int rc = 0;
    const char* proto;
    bool enable = false;          // expected status of the pppd process as described in the new settings in args
    bool new_parameters = false;  // are the new settings in args different from the one that are currently appplied?

    proto = GETP_CHAR(args, "proto");
    if(proto == NULL) {
        SAH_TRACEZ_ERROR(ME, "proto should be set");
        rc = EXIT_OPTION_ERROR;
        goto exit;
    }

    enable = strcmp(proto, "pppoe") == 0;

    rc = amxc_var_compare(args, current_options, &res);
    when_failed(rc, exit);
    new_parameters = (res != 0);

    if((current_state == STATE_WAIT_FOR_RESTART) && new_parameters) {
        if(enable) {
            /*
             * During the interval of the restart timer we received new settings.
             * Instead of using the currently in place settings for the new connection use the ones that we just received.
             */
            update_restart_args(args);
        } else {
            /*
             * Even though the restart timer is running to eventually restart the connection on the timer timeout,
             * the new settings require that the process should be stopped.
             */
            amxp_timer_stop(restart_timer);
        }
    }

    if(reset) {
        switch(set_state_on_reset()) {
        case STATE_STARTED:
            SAH_TRACEZ_INFO(ME, "pppd not running yet, starting pppd...");
            rc = start(args);
            break;
        case STATE_RESET:
            SAH_TRACEZ_INFO(ME, "Resetting pppd...");
            if(amxc_var_is_null(restart_args)) {
                amxc_var_new(&restart_args);
                amxc_var_copy(restart_args, args);
            }
            rc = stop();
            break;
        }
    } else {
        int previous_state = current_state;
        switch(set_state_on_settings_changed(enable, new_parameters)) {
        case STATE_STARTED:
            if(previous_state != STATE_STARTED) {
                SAH_TRACEZ_INFO(ME, "Starting pppd...");
                rc = start(args);
            }
            break;
        case STATE_STOPPING:
            SAH_TRACEZ_INFO(ME, "Stopping pppd...");
            if(enable) {
                update_restart_args(args);
            }
            rc = stop();
            break;
        }
    }

exit:
    if(rc != 0) {
        set_state(STATE_STOPPED);
    }
    SAH_TRACEZ_OUT(ME);
    return rc;
}

/*
 * We no longer wish to keep the connection going, cleanup current_options
 */
static void cleanup_state(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_delete(&current_options);
    amxc_var_delete(&restart_args);
    cleanup_timepoints(&authentication_failed_schema);
    restart_auth_failed_schema = true; // next time the connection is started the auth schema needs to start from beginning as well
    set_state(STATE_STOPPED);
    SAH_TRACEZ_OUT(ME);
}

/*
 * Called by the signal handlers after receiving a registered signal.
 * Equivalent to the handle_events function in pppd.
 */
int pppd_handle_events(void) {
    SAH_TRACEZ_IN(ME);
    handle_events(BLOCKING);
    if(BLOCKING) {
        get_input();
    }
    react_on_handled_events();
    if((current_state == STATE_STOPPING) && (current_phase == PHASE_DEAD)) {
        cleanup_state();
    }
    SAH_TRACEZ_OUT(ME);
    return 0;
}

/*
 * This function is invoked when the pppd process dies.
 * It might be necessary to restart the process again.
 */
void handle_died_ppp(UNUSED const char* const sig_name,
                     UNUSED const amxc_var_t* const data,
                     UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    int start_delay_ms = 0;
    int rv = -1;

    if(current_state == STATE_RESET) {
        amxc_var_add_key(bool, restart_args, "reset", true);
    }

    set_state(STATE_DIED);

    rv = cleanup_connection(BLOCKING);
    cleanup_children(BLOCKING);
    cleanup();
    remove_all_fds();
    sys_close();
    close(fd_devnull);

    free(nic_name);
    nic_name = NULL;

    free(interface_name);
    interface_name = NULL;

    free(lcp_echo_restart_delay);
    lcp_echo_restart_delay = NULL;

    restart_auth_failed_schema = (status != EXIT_PEER_AUTH_FAILED && status != EXIT_AUTH_TOPEER_FAILED);

    if((rv != 0) && (restart_args == NULL)) {
        /* We no longer wish to keep the connection going, cleanup current_options */
        cleanup_state();
        goto exit;
    }

    // Restart process
    // In some situations we want to delay starting again.
    switch(status) {
    case EXIT_CONNECT_FAILED:       /* Discovery (PADI -> PADO -> PADR -> PADS) failed */
    case EXIT_NEGOTIATION_FAILED:   /* IPCP/LCP Conf request failed */
    case EXIT_PEER_AUTH_NO_REPLY:   /* No reply on authentication requests */
    case EXIT_AUTH_TOPEER_NO_REPLY: /* No reply on authentication requests */
        start_delay_ms += restart_delay;
        break;
    case EXIT_PEER_AUTH_FAILED:     /* Authentication failure */
    case EXIT_AUTH_TOPEER_FAILED:
    {
        int auth_failed_delay = next(authentication_failed_schema);
        when_true_trace(auth_failed_delay == ERROR_NO_TIMEPOINT_LEFT, exit, ERROR, "No more authentication timepoints left, stopping");
        start_delay_ms += auth_failed_delay;
        break;
    }
    case EXIT_PEER_DEAD:            /* No reply on LCP Echoes */
        start_delay_ms += get_timeout(lcp_echo_restart_delay);
        break;
    }

    if(start_delay_ms != 0) {
        SAH_TRACEZ_INFO(ME, "Delaying startup of pppd process by %.4lf second%s", ((double) start_delay_ms) / 1000.0, start_delay_ms >= 2 ? "s" : "");
    }
    set_state(STATE_WAIT_FOR_RESTART);
    amxp_timer_start(restart_timer, start_delay_ms);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}
