/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <syslog.h>
#include <stdlib.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <pppd/pppd.h>

#include "ppp.h"
#include "pppd_utils.h"

#define str_not_empty(x) (((x) == NULL || *(x) == 0) == false)
#define SAFE_GET_CHAR(var) var = GET_CHAR(args, #var); when_null(var, exit)

/* This function generates a random timeout in milliseconds, provided a value between min_sec and max_sec.
 * The precision_ms argument can be used to increase the precision of this random timeout.
 * E.g: min_sec = 10 ; max_sec = 15 ; precision_ms = 100 -> 12500ms
 * E.g: min_sec =  1 ; max_sec =  2 ; precision_ms =   1 ->  1385ms
 */
int get_random_timeout_ms(int min_sec, int max_sec, int precision_ms) {
    SAH_TRACEZ_IN(ME);
    int range = (max_sec - min_sec) ? max_sec - min_sec : 1;
    int val = rand() % (range * 1000) + min_sec * 1000;

    val = (val / precision_ms) * precision_ms;
    return val;
    SAH_TRACEZ_OUT(ME);
}

/* This function expects a csv string with 1 or 3 integer values:
 * 1 integer value:  Constant delay, return as is.
 * 3 integer values: Random delay:
 *   - First value:  Minimum delay in seconds
 *   - Second value: Maximum delay in seconds
 *   - Third value:  Precisssion in milliseconds (see get_random_timeout_ms)
 */
int get_timeout(const char* delay_schema) {
    SAH_TRACEZ_IN(ME);
    int i = 0;
    size_t j = 0;
    int timeout = 0;
    int arr[3] = {0}; /* 0: minimum value; 1: maximum value; 2: millisecond precision */

    amxc_var_t delay_values;
    amxc_string_t delay_values_csv;
    amxc_var_init(&delay_values);
    amxc_string_init(&delay_values_csv, 0);
    amxc_string_set(&delay_values_csv, delay_schema);
    when_failed(amxc_string_csv_to_var(&delay_values_csv, &delay_values, NULL), exit);

    amxc_var_for_each(mod_dir, &delay_values) {
        const char* str_value = NULL;

        when_true(i > 2, exit);

        str_value = GET_CHAR(mod_dir, NULL);
        for(j = 0; j < strlen(str_value); j++) {
            when_false(isdigit(str_value[j]), exit)
        }

        arr[i] = GET_UINT32(mod_dir, NULL);
        i++;
    }

    switch(i) {
    case 1:
        timeout = arr[0];
        break;
    case 3:
        timeout = get_random_timeout_ms(arr[0], arr[1], arr[2]);
        break;
    }

exit:
    amxc_string_clean(&delay_values_csv);
    amxc_var_clean(&delay_values);
    SAH_TRACEZ_OUT(ME);
    return timeout;
}

/*
 * Translates the phase enum value of the pppd process
 * to a human readable string.
 */
const char* phase_to_string(int nr) {
    static const char* PHASES[] = {"dead", "initialize", "serialconn", "dormant",
        "establish", "authenticate", "callback", "network",
        "running", "terminate", "disconnect", "holdoff", "master"};

    return (nr >= 0 && nr <= PHASE_MASTER) ? PHASES[nr] : NULL;
}

const char* status_to_string(int nr) {
    static const char* STATUSES[] = {"ok", "fatal error", "option error", "not root",
        "no kernel support", "user request", "lock failed",
        "open_failed", "connect failed", "ptycmd failed", "negotiation failed",
        "peer authentication failed", "idle timeout", "connect time", "callback",
        "peer dead", "hangup", "loopback", "init failed", "authentication to peer failed",
        "traffic limit", "cnid authentication failed", "peer authentication no reply", "authentication to peer no reply"};

    return (nr >= 0 && nr <= EXIT_AUTH_TOPEER_NO_REPLY) ? STATUSES[nr] : NULL;
}

static const char* get_nic_name(amxc_var_t* args, amxc_string_t* buf) {
    SAH_TRACEZ_IN(ME);
    const char* result = NULL;
    when_null(args, exit);
    amxc_string_appendf(buf, "nic-%s", GETP_CHAR(args, "ifname"));
    result = amxc_string_get(buf, 0);
exit:
    SAH_TRACEZ_OUT(ME);
    return result;
}

/*
 * Helper function that adds an entry in the wordlist structure known by the pppd process.
 * The word is added to the front of the list.
 */
static int add_word_to_wordlist(struct wordlist** list, const char* word) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;

    struct wordlist* new_entry;

    new_entry = (struct wordlist*) malloc(sizeof(struct wordlist) + strlen(word) + 1);
    when_null(new_entry, exit);

    new_entry->word = (char*) (new_entry + 1);
    strcpy(new_entry->word, word);

    new_entry->next = (*list);
    (*list) = new_entry;
    ret = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

/*
 * Release memory allocated for a wordlist.
 */
static void free_wordlist(struct wordlist* wp) {
    SAH_TRACEZ_IN(ME);
    struct wordlist* next = NULL;
    while(wp != NULL) {
        next = wp->next;
        free(wp);
        wp = next;
    }
    SAH_TRACEZ_OUT(ME);
}

/*
 * Log wordlist that contains the pppd process settings.
 */
static void log_wordlist(struct wordlist* wp) {
    SAH_TRACEZ_INFO(ME, "Currently set options:");

    while(wp != NULL) {
        if(wp->word != NULL) {
            SAH_TRACEZ_INFO(ME, "%s", wp->word);
        }
        wp = wp->next;
    }
}

/*
 * Add a key value pair to the pppd options wordlist.
 * Value is optional.
 */
static void pppd_add_option(struct wordlist** wp, const char* key, const char* value) {
    when_null(key, exit);

    if(value != NULL) {
        when_failed(add_word_to_wordlist(wp, value), exit);
    }

    if(key != NULL) {
        add_word_to_wordlist(wp, key);
    }

exit:
    return;
}

/*
 * Sets the options of the pppd process based on
 * data in the variant.
 */
int pppd_set_options(amxc_var_t* args) {
    SAH_TRACEZ_IN(ME);
    int rc = EXIT_OPTION_ERROR;
    const char* proto = NULL;
    const char* username = NULL;
    const char* password = NULL;
    const char* ac_name = NULL;
    const char* service_name = NULL;
    const char* padi_interval = NULL;
    const char* padr_interval = NULL;
    amxc_string_t keepalive;
    amxc_llist_t list;
    amxc_string_t* buf = NULL;
    const char* reason = NULL;
    const char* lcp_echo_retry = NULL;
    const char* lcp_echo_interval = NULL;
    const char* max_mru_size = NULL;
    const char* lcp_conf_req_interval = NULL;
    const char* lcp_term_interval = NULL;
    const char* ipcp_conf_req_interval = NULL;
    const char* ipcp_term_req_interval = NULL;
    const char* interface = NULL;
    const char* auth_interval = NULL;
    const char* auth_protocol = NULL;
    bool ipv4 = false;
    bool ipv6 = false;

    struct wordlist* wordlist = NULL;
    amxc_llist_init(&list);
    amxc_string_new(&buf, 0);

    when_null(args, exit);

    ipv4 = GET_BOOL(args, "ipv4");
    ipv6 = GET_BOOL(args, "ipv6");

    SAFE_GET_CHAR(proto);
    when_true(strcmp(proto, "none") == 0, exit);

    SAFE_GET_CHAR(username);
    SAFE_GET_CHAR(password);
    SAFE_GET_CHAR(ac_name);
    SAFE_GET_CHAR(service_name);
    SAFE_GET_CHAR(padi_interval);
    SAFE_GET_CHAR(padr_interval);
    SAFE_GET_CHAR(lcp_conf_req_interval);
    SAFE_GET_CHAR(lcp_term_interval);
    SAFE_GET_CHAR(ipcp_conf_req_interval);
    SAFE_GET_CHAR(ipcp_term_req_interval);
    SAFE_GET_CHAR(auth_interval);
    SAFE_GET_CHAR(auth_protocol);

    when_null(GET_CHAR(args, "keepalive"), exit);
    when_null(GET_CHAR(args, "ifname"), exit);

    interface = get_nic_name(args, buf);
    when_null(interface, exit);

    amxc_string_init(&keepalive, 0);
    amxc_string_appendf(&keepalive, "%s", GET_CHAR(args, "keepalive"));
    amxc_string_split_word(&keepalive, &list, &reason);
    amxc_string_clean(&keepalive);

    lcp_echo_retry = amxc_string_get_text_from_llist(&list, 0);
    lcp_echo_interval = amxc_string_get_text_from_llist(&list, 2);
    max_mru_size = amxc_string_get_text_from_llist(&list, 4);

    /* PPPoe plugin settings.
     * Order is important here since new values in the list are added in front.
     */
    pppd_add_option(&wordlist, interface, NULL);
    pppd_add_option(&wordlist, "pppoe-padi-interval-schema", padi_interval);
    pppd_add_option(&wordlist, "pppoe-padr-interval-schema", padr_interval);
    if(str_not_empty(service_name)) {
        // Only add service_name option if the string has a value (non-empty string).
        pppd_add_option(&wordlist, "rp_pppoe_service", service_name);
    }
    if(str_not_empty(ac_name)) {
        // Only add ac_name option if the string has a value (non-empty string).
        pppd_add_option(&wordlist, "rp_pppoe_ac", ac_name);
    }
    pppd_add_option(&wordlist, "rp_pppoe_verbose", "1");
    // "plugin" entry has to be the last of the PPPoe plugin settings.
    pppd_add_option(&wordlist, "plugin", "rp-pppoe.so");

    /* Main pppd settings.
     * Based on mod-ppp-direct, up/down scripts are not needed for this implementation.
     */
    pppd_add_option(&wordlist, "mru", max_mru_size);
    pppd_add_option(&wordlist, "mtu", "1500");
    pppd_add_option(&wordlist, "password", password);
    pppd_add_option(&wordlist, "user", username);
    pppd_add_option(&wordlist, "maxfail", "0");
    pppd_add_option(&wordlist, "persist", NULL);
    pppd_add_option(&wordlist, "holdoff", "0");
    pppd_add_option(&wordlist, "usepeerdns", NULL);
    pppd_add_option(&wordlist, "nodefaultroute", NULL);
    if(ipv4) {
        pppd_add_option(&wordlist, "+ip", NULL);
        pppd_add_option(&wordlist, "ipcp-conf-req-interval-schema", ipcp_conf_req_interval);
        pppd_add_option(&wordlist, "ipcp-term-interval-schema", ipcp_term_req_interval);
    } else {
        pppd_add_option(&wordlist, "-ip", NULL);
    }

    if(ipv6) {
        pppd_add_option(&wordlist, "+ipv6", NULL);
        pppd_add_option(&wordlist, "ipv6cp-conf-req-interval-schema", ipcp_conf_req_interval);
        pppd_add_option(&wordlist, "ipv6cp-term-interval-schema", ipcp_term_req_interval);
    } else {
        pppd_add_option(&wordlist, "-ipv6", NULL);
    }
    pppd_add_option(&wordlist, "lcp-conf-req-interval-schema", lcp_conf_req_interval);
    pppd_add_option(&wordlist, "lcp-term-interval-schema", lcp_term_interval);
    pppd_add_option(&wordlist, "lcp-echo-failure", lcp_echo_retry);
    pppd_add_option(&wordlist, "lcp-echo-interval", lcp_echo_interval);
    pppd_add_option(&wordlist, "ifname", interface_name);
    pppd_add_option(&wordlist, "ipparam", "wan");
    pppd_add_option(&wordlist, "nodetach", NULL);
    if(sahTraceZoneLevel(sahTraceGetZone(ME)) >= TRACE_LEVEL_INFO) {
        // automatically add pppd debug traces when our module has a high enough tracezone level.
        pppd_add_option(&wordlist, "debug", NULL);
    }

    if(strcmp(auth_protocol, "PAP") == 0) {
        pppd_add_option(&wordlist, "pap-interval-schema", auth_interval);
        pppd_add_option(&wordlist, "refuse-chap", NULL);
    } else if(strcmp(auth_protocol, "CHAP") == 0) {
        pppd_add_option(&wordlist, "chap-interval-schema", auth_interval);
        pppd_add_option(&wordlist, "refuse-pap", NULL);
    } else {
        pppd_add_option(&wordlist, "pap-interval-schema", auth_interval);
        pppd_add_option(&wordlist, "chap-interval-schema", auth_interval);
    }
    /* Currently only support PAP and CHAP */
    pppd_add_option(&wordlist, "refuse-mschap", NULL);
    pppd_add_option(&wordlist, "refuse-mschap-v2", NULL);
    pppd_add_option(&wordlist, "refuse-eap", NULL);

    if(options_from_list(wordlist, 1) != 1) {
        // options_from_list returns 1 if there is no error instead of 0.
        goto exit;
    }
    log_wordlist(wordlist);

    rc = 0;

exit:
    free_wordlist(wordlist);
    amxc_llist_clean(&list, amxc_string_list_it_free);
    amxc_string_delete(&buf);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

/*
 * Adds a list of file descriptors that the pppd process uses
 * to the ambiorix event loop.
 */
void add_fds(amxc_var_t* fd_list, const char* handler) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_true_trace(BLOCKING, exit, ERROR, "pppd is configured as blocking, not adding fds to event loop!");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    if(amxc_var_type_of(fd_list) == AMXC_VAR_ID_LIST) {
        amxc_var_add_key(amxc_llist_t, &args, "file_descriptors", amxc_var_constcast(amxc_llist_t, fd_list));
        amxc_var_add_key(cstring_t, &args, "handler", handler);

        SAH_TRACEZ_INFO(ME, "mod-ppp-daemon executes core's event-loop-add-fd");
        amxm_execute_function(NULL, MOD_CORE, "event-loop-add-fd", &args, &ret);
    } else {
        SAH_TRACEZ_ERROR(ME, "expected a list of file descriptors, got %s", amxc_var_type_name_of(fd_list));
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);
}


void remove_fds(amxc_var_t* fd_list) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_true_trace(BLOCKING, exit, ERROR, "pppd is configured as blocking, not removing fds to event loop!");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    if(amxc_var_type_of(fd_list) == AMXC_VAR_ID_LIST) {
        amxc_var_add_key(amxc_llist_t, &args, "file_descriptors", amxc_var_constcast(amxc_llist_t, fd_list));

        SAH_TRACEZ_INFO(ME, "mod-ppp-daemon executes core's event-loop-remove-fd");
        amxm_execute_function(NULL, MOD_CORE, "event-loop-remove-fd", &args, &ret);
    } else {
        SAH_TRACEZ_ERROR(ME, "expected a list of file descriptors, got %s", amxc_var_type_name_of(fd_list));
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);

}

/*
 * Remove all file descriptors that the pppd process uses
 * from the ambiorix event loop.
 */
void remove_all_fds(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    SAH_TRACEZ_INFO(ME, "mod-ppp-daemon executes core's event-loop-cleanup-fds");
    amxm_execute_function(NULL, MOD_CORE, "event-loop-cleanup-fds", &args, &ret);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);
}

/*
 * Retrieve the amount of listening file descriptors that are currently
 * configured in the ambiorix event loop.
 */
int get_nr_fds(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t args;
    amxc_var_t ret;
    int nr_fds = 0;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set(int32_t, &ret, 0);
    SAH_TRACEZ_INFO(ME, "mod-ppp-daemon executes core's event-loop-count-fds");
    amxm_execute_function(NULL, MOD_CORE, "event-loop-count-fds", &args, &ret);

    nr_fds = GET_INT32(&ret, NULL);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    SAH_TRACEZ_OUT(ME);
    return nr_fds;
}
