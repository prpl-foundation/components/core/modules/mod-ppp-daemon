/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <debug/sahtrace.h>

#include "ppp.h"
#include "pppd_state.h"

int current_state = STATE_UNCONFIGURED;

const char* state_to_string(int nr) {
    static const char* STATES[] = {"unconfigured", "started", "stopping", "died",
        "wait for restart", "ready for restart", "stopped", "reset"};
    return (nr >= 0 && nr <= STATE_MAX) ? STATES[nr] : NULL;
}

/*
 * Set a new state based on the current_state when the pppd process needs to be started in case of a reset.
 */
int set_state_on_reset(void) {
    SAH_TRACEZ_IN(ME);
    switch(current_state) {
    case STATE_UNCONFIGURED:
    // FALL THROUGH
    case STATE_STOPPED:
    // FALL THROUGH
    case STATE_READY_FOR_RESTART:
        // Currently there is no established PPP connnection so we can just start one.
        set_state(STATE_STARTED);
        break;
    case STATE_STARTED:
        // The established PPP connection needs to be stopped first before it can be started again.
        set_state(STATE_RESET);
        break;
    }

    return current_state;
    SAH_TRACEZ_OUT(ME);
}

/*
 * Set a new state based on the current_state when settings are changed.
 */
int set_state_on_settings_changed(bool enable, bool new_parameters) {
    SAH_TRACEZ_IN(ME);
    if(((current_state == STATE_READY_FOR_RESTART) || (current_state == STATE_WAIT_FOR_RESTART)) && (enable == false)) {
        // When the connection needs to be stopped and the restart timer is started or expired immediately STOP.
        set_state(STATE_STOPPING);
    } else {
        switch(current_state) {
        case STATE_UNCONFIGURED:
        // FALL THROUGH
        case STATE_READY_FOR_RESTART:
        // FALL THROUGH
        case STATE_STOPPED:
            // Currently there is no established PPP connnection.
            if(enable) {
                // In case the connection needs to be started, set the state to STARTED.
                set_state(STATE_STARTED);
            }
            break;
        case STATE_STARTED:
            // The PPP connection is already established.
            if(new_parameters) {
                // In case there are new settings, stop the connection first.
                // Note: Based on the new parameters the connection might be started again once the connection is stopped.
                set_state(STATE_STOPPING);
            }
            break;
        }
    }

    SAH_TRACEZ_OUT(ME);
    return current_state;
}

void set_state(int new_state) {
    SAH_TRACEZ_IN(ME);
    if(new_state <= STATE_MAX) {
        SAH_TRACEZ_INFO(ME, "changing state from %s to %s", state_to_string(current_state), state_to_string(new_state));
        current_state = new_state;
    }
    SAH_TRACEZ_OUT(ME);
}