/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <debug/sahtrace.h>

#include <pppd/pppd.h>

#include "ppp.h"
#include "pppd_actions.h"
#include "pppd_callbacks.h"
#include "pppd_dm.h"
#include "pppd_utils.h"

int current_phase = PHASE_DEAD;

#define PPP_CONN_STATUS_UNCONFIGURED       "Unconfigured"
#define PPP_CONN_STATUS_CONNECTING         "Connecting"
#define PPP_CONN_STATUS_AUTHENTICATING     "Authenticating"
#define PPP_CONN_STATUS_CONNECTED          "Connected"
#define PPP_CONN_STATUS_PENDING_DISCONNECT "PendingDisconnect"
#define PPP_CONN_STATUS_DISCONNECTING      "Disconnecting"
#define PPP_CONN_STATUS_DISCONNECTED       "Disconnected"

#define PPP_STATUS_UP                      "Up"
#define PPP_STATUS_DOWN                    "Down"
#define PPP_STATUS_UNKNOWN                 "Unknown"
#define PPP_STATUS_DORMANT                 "Dormant"
#define PPP_STATUS_NOT_PRESENT             "NotPresent"
#define PPP_STATUS_LOWER_LAYER_DOWN        "LowerLayerDown"
#define PPP_STATUS_ERROR                   "Error"

/*
 * Hooks
 * These hooks can be seen as callback functions that are called in certain situations.
 */

void ip_up(void) {
    SAH_TRACEZ_IN(ME);
    set_ipcp(true);
    SAH_TRACEZ_OUT(ME);
}

void ip_down(void) {
    SAH_TRACEZ_IN(ME);
    set_ipcp(false);
    SAH_TRACEZ_OUT(ME);
}

void ipv6_up(void) {
    SAH_TRACEZ_IN(ME);
    set_ipcpv6(true);
    SAH_TRACEZ_OUT(ME);
}

void ipv6_down(void) {
    SAH_TRACEZ_IN(ME);
    set_ipcpv6(false);
    SAH_TRACEZ_OUT(ME);
}

void encryption_protocol(char* proto) {
    SAH_TRACEZ_IN(ME);
    set_key_value(PPP_ENCRYPTION, proto);
    SAH_TRACEZ_OUT(ME);
}

void authentication_protocol(char* proto) {
    SAH_TRACEZ_IN(ME);
    set_key_value(PPP_AUTHENTICATION, proto);
    SAH_TRACEZ_OUT(ME);
}

void discovery_succeeded(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t fd_list;
    amxc_var_init(&fd_list);

    set_key_value_int(PPP_PPPOE_SESSION_ID, ppp_session_number);

    when_true(BLOCKING, exit);

    /*
     * When PPP Discovery (PADI, PADO, PADR, PADS packets) succeeds we need to start the negotiation phase.
     * The discovery phase uses the discovery_fd and all other phases use the ppp_fd and ppp_dev_fd.
     * We need to add the ppp_fd and ppp_dev_fd file descriptors to the amx eventloop.
     */
    amxc_var_set_type(&fd_list, AMXC_VAR_ID_LIST);
    amxc_var_add(fd_t, &fd_list, ppp_fd);
    amxc_var_add(fd_t, &fd_list, ppp_dev_fd);
    SAH_TRACEZ_INFO(ME, "Adding ppp_fd %d to amx eventloop", ppp_fd);
    SAH_TRACEZ_INFO(ME, "Adding ppp_dev_fd %d to amx eventloop", ppp_dev_fd);
    add_fds(&fd_list, "get-input");

exit:
    amxc_var_clean(&fd_list);
    SAH_TRACEZ_OUT(ME);
}

void discovery_stopped(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t fd_list;
    int* discovery_fd = get_discovery_fd();

    amxc_var_init(&fd_list);
    amxc_var_set_type(&fd_list, AMXC_VAR_ID_LIST);

    amxc_var_add(fd_t, &fd_list, *discovery_fd);
    SAH_TRACEZ_INFO(ME, "Removing discovery_fd %d from amx eventloop", *discovery_fd);
    remove_fds(&fd_list);
    *discovery_fd = -1;

    amxc_var_clean(&fd_list);
    SAH_TRACEZ_OUT(ME);
}

void mru_changed(int mru) {
    SAH_TRACEZ_IN(ME);
    set_key_value_int(PPP_CURRENT_MRU_SIZE, mru);
    SAH_TRACEZ_OUT(ME);
}

// Already present hooks in official codebase
void (* ip_up_hook)(void) = ip_up;
void (* ip_down_hook)(void) = ip_down;
void (* ipv6_up_hook)(void) = ipv6_up;
void (* ipv6_down_hook)(void) = ipv6_down;

// Added hooks to get more data out of the pppd code
void (* encryption_protocol_hook)(char* proto) = encryption_protocol;
void (* authentication_protocol_hook)(char* proto) = authentication_protocol;
void (* discovery_succeeded_hook)(void) = discovery_succeeded;
void (* discovery_stopped_hook)(void) = discovery_stopped;
void (* mru_changed_hook)(int) = mru_changed;

/* Notifier callbacks
 * Certain events have their own notifier structure.
 * They work in a similar way as the hooks.
 */

/*
 * When the pppd process enters a new state we want to take note of this
 * and handle accordingly (update dm, emit events, ...)
 */
void phase_change(UNUSED void* opaque, const int new_phase) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "pppd phase change: %s (%d) -> %s (%d)",
                    phase_to_string(current_phase), current_phase,
                    phase_to_string(new_phase), new_phase);

    if(new_phase == PHASE_DORMANT) {
        set_key_value(PPP_STATUS, PPP_STATUS_DORMANT);
    }

    if(new_phase > current_phase) {
        // Startup behaviour
        switch(new_phase) {
        case PHASE_INITIALIZE:
            set_key_value(PPP_STATUS, PPP_STATUS_LOWER_LAYER_DOWN);                  break;
        case PHASE_SERIALCONN:
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_CONNECTING);         break;
        case PHASE_AUTHENTICATE:
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_AUTHENTICATING);     break;
        case PHASE_RUNNING:
            set_key_value(PPP_STATUS, PPP_STATUS_UP);
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_CONNECTED);
            set_key_value(PPP_LASTCONNECTIONERROR, PPP_LAST_CONN_ERROR_NONE);        break;
        case PHASE_TERMINATE:
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_PENDING_DISCONNECT); break;
        case PHASE_DISCONNECT:
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_DISCONNECTING);      break;
        case PHASE_HOLDOFF: // falltrough
        case PHASE_MASTER:
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_DISCONNECTED);       break;
        default:                                                                     break;
        }
    } else if(new_phase < current_phase) {
        // Something went wrong (e.g: link loss)
        if((current_phase > PHASE_AUTHENTICATE) && (new_phase <= PHASE_AUTHENTICATE)) {
            set_key_value(PPP_AUTHENTICATION, "None");
        }
        if(new_phase == PHASE_DEAD) {
            set_key_value(PPP_STATUS, PPP_STATUS_DOWN);
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_DISCONNECTED);
            set_key_value_int(PPP_PPPOE_SESSION_ID, 1);
            current_phase = new_phase;
            set_last_connection_error(status);
            amxp_sigmngr_emit_signal(NULL, "ppp_died", NULL);
        } else if(new_phase == PHASE_ESTABLISH) {
            SAH_TRACEZ_ERROR(ME, "LCP has gone down, reconnecting...");
            set_key_value(PPP_STATUS, PPP_STATUS_LOWER_LAYER_DOWN);
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_CONNECTING);
        } else if(new_phase == PHASE_NETWORK) {
            SAH_TRACEZ_ERROR(ME, "Network Protocol has gone down, reauthenticating...");
            set_key_value(PPP_STATUS, PPP_STATUS_LOWER_LAYER_DOWN);
            set_key_value(PPP_CONNECTIONSTATUS, PPP_CONN_STATUS_AUTHENTICATING);
        }
    }

    if(new_phase != PHASE_DEAD) {
        current_phase = new_phase;
    }
    SAH_TRACEZ_OUT(ME);
}
