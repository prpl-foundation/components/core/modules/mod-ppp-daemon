/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <debug/sahtrace.h>

#include <pppd/pppd.h>

#include "ppp.h"
#include "pppd_actions.h"
#include "pppd_callbacks.h"
#include "pppd_dm.h"
#include "pppd_signals.h"
#include "pppd_state.h"

amxp_timer_t* timeout_timer;

/*
 * Called when the timeout_timer expires.
 * The pppd process will check if any internal timeouts (e.g: resend CONFREQ) occurred.
 */
static void check_timeouts(UNUSED amxp_timer_t* const t,
                           UNUSED void* data) {
    calltimeout();
}

/*
 * Start/Stop/Restart the pppd process based on the variant data.
 */
int update_config(UNUSED const char* function_name,
                  amxc_var_t* args,
                  UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = manage_process(args, false);
    SAH_TRACEZ_OUT(ME);

    return rv;
}

static int reset(UNUSED const char* function_name,
                 amxc_var_t* args,
                 UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = manage_process(args, true);
    SAH_TRACEZ_OUT(ME);

    return rv;
}

static int terminate_connection(UNUSED const char* function_name,
                                UNUSED amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "Terminating PPP connection");
    amxp_sigmngr_trigger_signal(NULL, strsignal(SIGINT), NULL);
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int ppp_handle_script(UNUSED const char* function_name,
                             UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "ppp-mod-daemon implementation doesn't use up/down scripts.");
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int use_netdev_subscriptions(UNUSED const char* function_name,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    // We only use data from the pppd process, ignore all netdev information.
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int use_timers(UNUSED const char* function_name,
                      UNUSED amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    // We don't need timers to decide wether ppp started correctly or not,
    // we have this information available in the pppd process.
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return 0;
}

/*
 * Each time data is available on at least one of the pppd process file descriptors (negotiation and data transmission phase) this function is called.
 * For incoming data in the discovery phase we use the ppp_handle_discovery function.
 */
int ppp_get_input(UNUSED const char* function_name,
                  UNUSED amxc_var_t* args,
                  UNUSED amxc_var_t* ret) {
    calltimeout();
    get_input();
    return 0;
}

/*
 * Each time data is available on the discovery socket of the the pppd process this function is called.
 */
int ppp_handle_discovery(UNUSED const char* function_name,
                         UNUSED amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    calltimeout();
    if(the_channel != NULL) {
        the_channel->handle_discovery();
    }
    return 0;
}

static void init_signal_handler(void) {
    SAH_TRACEZ_IN(ME);
    amxp_sigmngr_add_signal(NULL, "ppp_died");

    amxp_syssig_enable(SIGHUP, true);
    amxp_syssig_enable(SIGINT, true);

    amxp_slot_connect(NULL, strsignal(SIGHUP), NULL, pppd_hup, NULL);
    amxp_slot_connect(NULL, strsignal(SIGINT), NULL, pppd_term, NULL);

    amxp_slot_connect(NULL, strsignal(SIGCHLD), NULL, pppd_chld, NULL);

    amxp_slot_connect(NULL, strsignal(SIGUSR1), NULL, pppd_usr1, NULL);  /* Toggle debug flag */
    amxp_slot_connect(NULL, strsignal(SIGUSR2), NULL, pppd_usr2, NULL);  /* Reopen CCP */
    amxp_slot_connect(NULL, "ppp_died", NULL, handle_died_ppp, NULL);
    SAH_TRACEZ_OUT(ME);
}

static AMXM_CONSTRUCTOR ppp_start(void) {
    SAH_TRACEZ_IN(ME);
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_PPP_CTRL);
    amxm_module_add_function(mod, "update-config", update_config);
    amxm_module_add_function(mod, "reset", reset);
    amxm_module_add_function(mod, "stop", terminate_connection);
    amxm_module_add_function(mod, "handle-script", ppp_handle_script);
    amxm_module_add_function(mod, "use-netdev-subscriptions", use_netdev_subscriptions);
    amxm_module_add_function(mod, "use-timers", use_timers);
    amxm_module_add_function(mod, "get-input", ppp_get_input);
    amxm_module_add_function(mod, "handle-discovery", ppp_handle_discovery);

    /* srand needed to seed time() in get_random_timeout_ms() */
    srand(time(NULL));

    init_signal_handler();
    add_notifier(&phasechange, phase_change, NULL);
    amxp_timer_new(&timeout_timer, check_timeouts, NULL);
    amxp_timer_new(&restart_timer, restart_timer_timeout, NULL);

    amxp_timer_set_interval(timeout_timer, 100);
    amxp_timer_start(timeout_timer, 100);

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static AMXM_DESTRUCTOR ppp_stop(void) {
    SAH_TRACEZ_IN(ME);
    amxp_timer_delete(&timeout_timer);
    amxp_timer_delete(&restart_timer);
    SAH_TRACEZ_OUT(ME);
    return 0;
}
