# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.5.11 - 2024-06-13(09:11:46 +0000)

### Other

- - [tr181-ppp] Switching from ppp6 to ppp4 is not working

## Release v0.5.10 - 2024-05-24(12:34:21 +0000)

### Fixes

-  [tr181-ppp] Open filedescriptors increase when PPP connection fails to start

## Release v0.5.9 - 2024-05-23(19:50:52 +0000)

### Fixes

-  [PPP]No PADT packet received

## Release v0.5.8 - 2024-05-22(07:21:25 +0000)

### Fixes

-  [tr181-ppp] IPCP (ppp4) is not working

## Release v0.5.7 - 2024-05-09(07:35:51 +0000)

### Fixes

- [tr181-ppp] IPCP IPCP enable = 0 not working

## Release v0.5.6 - 2024-01-31(12:51:56 +0000)

### Fixes

- - [tr181-ppp] high cpu usage

## Release v0.5.5 - 2023-10-04(07:47:02 +0000)

### Other

- Change ppp runtime dependency to ppp-prpl

## Release v0.5.4 - 2023-08-23(11:19:13 +0000)

### Other

- Opensource component

## Release v0.5.3 - 2023-08-22(08:08:15 +0000)

### Fixes

-  [mod-ppp-daemon] Not compiling with musl

## Release v0.5.2 - 2023-07-26(09:09:26 +0000)

### Fixes

- [PRPL][OJO]PPP not responsive after first authentication failure

## Release v0.5.1 - 2023-07-06(08:06:34 +0000)

### Fixes

-  [tr181-ppp] update to username is ignored after failed restart of pppd

## Release v0.5.0 - 2023-06-23(09:36:00 +0000)

### New

- Perform a graceful shutdown when a stop signal is received

## Release v0.4.3 - 2023-06-15(08:15:14 +0000)

### Fixes

- [tr181-ppp] DM value MaxMRUSize ignored by plugin and CurrentMRUSize not updated in DM

## Release v0.4.2 - 2023-05-11(13:03:15 +0000)

### Fixes

- [tr181-ppp] Stopping tr181-ppp with init script is not stopping the plugin

## Release v0.4.1 - 2023-05-11(09:02:13 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v0.4.0 - 2023-04-24(09:33:39 +0000)

### New

- [amx][prpl][tr181-ppp] Retransmission schemes must be configurable for PPP IPCP/IP6CP/LCP

## Release v0.3.0 - 2023-03-20(14:48:09 +0000)

### New

- [amx][prpl][tr181-ppp] Retransmission schemes must be configurable for PPPoe PADI/PADR

## Release v0.2.5 - 2023-02-24(11:30:58 +0000)

### Fixes

- [tr181-ppp] PPP connection can be started when previous connection is not completely closed

## Release v0.2.4 - 2023-02-23(12:48:54 +0000)

### Fixes

- [TR181 PPP] pppoe-wan interface is created if eth0 comes up in demo_wanmode

## Release v0.2.3 - 2023-02-23(12:41:02 +0000)

### Fixes

- [tr181-ppp] PPPoE parameters in datamodel should be used

## Release v0.2.2 - 2023-02-14(12:17:32 +0000)

### Fixes

- [tr181-ppp] Sometimes there is no default route for ppp6

## Release v0.2.1 - 2023-02-02(15:09:36 +0000)

### Fixes

- [TR181 PPP] PPP blocking during PPPoE Discovery

## Release v0.2.0 - 2023-01-26(12:13:27 +0000)

### New

- [ppp][ipv6] It must be possible to support pppv6 (ip6cp) with the ppp plugin

## Release v0.1.0 - 2022-11-16(12:29:04 +0000)

### New

- [TR181-ppp] Create plugin to control pppd

