/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_PPP_DAEMON_H__)
#define __MOD_PPP_DAEMON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>

// Name of parameters in the datamodel
#define PPP_CTRL                "Controller"
#define PPP_ENCRYPTION          "EncryptionProtocol"
#define PPP_AUTHENTICATION      "AuthenticationProtocol"
#define PPP_STATUS              "Status"
#define PPP_CONNECTIONSTATUS    "ConnectionStatus"
#define PPP_LASTCONNECTIONERROR "LastConnectionError"
#define PPP_LOCAL_INTF_ID       "LocalInterfaceIdentifier"
#define PPP_REMOTE_INTF_ID      "RemoteInterfaceIdentifier"
#define PPP_PPPOE_SESSION_ID    "SessionID"
#define PPP_CURRENT_MRU_SIZE    "CurrentMRUSize"

#define PPP_LAST_CONN_ERROR_NONE "ERROR_NONE"

// Name of the module namespace that you want to use to execute the functions
#define MOD_PPP_CTRL "ppp-ctrl"

// Core mod name
#define MOD_CORE "mod-ppp"

#define ME "daemon-ppp"

#define BLOCKING false

int update_config(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int ppp_get_input(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int ppp_handle_discovery(const char* function_name, amxc_var_t* args, amxc_var_t* ret);

extern char* nic_name;             // e.g: eth0
extern char* interface_name;
extern int current_phase;          // internal state of the pppd process
extern int current_state;          // state of the module that manages the pppd process
extern amxp_timer_t* restart_timer;

#ifdef __cplusplus
}
#endif

#endif // __MOD_PPP_DAEMON_H__
