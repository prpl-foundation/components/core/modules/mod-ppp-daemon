/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../common/mock.h"
#include "test_module.h"

#include <pppd/pppd.h>
#include <pppd/generator.h>

#include "ppp.h"
#include "pppd_callbacks.h"
#include "pppd_actions.h"
#include "pppd_utils.h"
#include "pppd_state.h"


static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

#define MOD_CORE "mod-ppp"

bool ipv6 = false;
bool discovery_fails = false;
bool authenticate_fails = false;
bool lcp_echo_fails = false;

int amount_of_link_starts = 0;

static void handle_testing_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static void set_default_settings(amxc_var_t* settings, bool add_keepalive, bool add_auth_protocol) {
    amxc_var_add_key(cstring_t, settings, "username", "softathome");
    amxc_var_add_key(cstring_t, settings, "password", "softathome");
    amxc_var_add_key(cstring_t, settings, "ifname", "eth0");
    amxc_var_add_key(cstring_t, settings, "name", "pppoe-wan");
    amxc_var_add_key(cstring_t, settings, "keepalive_adaptive", "0");
    if(add_keepalive) {
        amxc_var_add_key(cstring_t, settings, "keepalive", "5 15 1400");
    }
    amxc_var_add_key(cstring_t, settings, "ac_name", "sah2r220");
    amxc_var_add_key(cstring_t, settings, "service_name", "best_service");
    amxc_var_add_key(cstring_t, settings, "padi_interval", "1,2,4,8,16,32,64,128,-1");
    amxc_var_add_key(cstring_t, settings, "padr_interval", "5,5,5,5,5");
    amxc_var_add_key(cstring_t, settings, "ipcp_conf_req_interval", "10000,10000,10000");
    amxc_var_add_key(cstring_t, settings, "ipcp_term_req_interval", "5000,5000");
    amxc_var_add_key(cstring_t, settings, "lcp_conf_req_interval", "10000,10000,10000");
    amxc_var_add_key(cstring_t, settings, "lcp_term_interval", "5000,5000");
    amxc_var_add_key(cstring_t, settings, "auth_interval", "5000,5000");
    amxc_var_add_key(cstring_t, settings, "auth_failed_interval", "10000,-1");
    if(add_auth_protocol) {
        amxc_var_add_key(cstring_t, settings, "auth_protocol", "Auto");
    }
    amxc_var_add_key(cstring_t, settings, "restart_delay", "5000");
    amxc_var_add_key(cstring_t, settings, "lcp_echo_restart_delay", "0,120,100");
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Ignore SIGALRM -> used for the timeout timer.
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    _dummy_main(0, &dm, &parser);

    /* needed for test_get_timeout() */
    srand(time(NULL));

    handle_testing_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
}

void test_has_correct_functions(UNUSED void** state) {
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "update-config"));
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "reset"));
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "stop"));
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "handle-script"));
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "use-netdev-subscriptions"));
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "use-timers"));
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "get-input"));
    assert_true(amxm_has_function("target_module", "ppp-ctrl", "handle-discovery"));
}

void test_use_netdev(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t data;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    assert_int_equal(amxm_execute_function("target_module", "ppp-ctrl", "use-netdev-subscriptions", &data, &ret), 0);
    handle_testing_events();

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_use_timers(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t data;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    assert_int_equal(amxm_execute_function("target_module", "ppp-ctrl", "use-timers", &data, &ret), 0);
    handle_testing_events();

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_handle_script(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t* data;

    amxc_var_init(&ret);
    data = dummy_read_json_from_file("test_data/test_handle_script_up.json");

    assert_int_equal(amxm_execute_function("target_module", "ppp-ctrl", "handle-script", data, &ret), 0);
    handle_testing_events();

    amxc_var_delete(&data);
    amxc_var_clean(&ret);
}

void test_reset(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t data;
    amxc_var_t* proto = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    // Start
    amxc_var_init(&data);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &data, "proto", "pppoe");
    set_default_settings(&data, true, true);
    amxc_var_add_key(bool, &data, "ipv6", ipv6);
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 1);

    // Reset
    assert_int_equal(manage_process(&data, true), 0);
    handle_testing_events();
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 2);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 2);

    amount_of_link_starts = 0;
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

void test_update_config(UNUSED void** state) {
    amxc_var_t settings;
    amxc_var_t* proto = NULL;
    amxc_var_t* keepalive = NULL;

    // Start without enable
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_init(&settings);
    assert_int_equal(update_config(NULL, &settings, NULL), EXIT_OPTION_ERROR);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 0);

    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    assert_int_equal(update_config(NULL, &settings, NULL), EXIT_OPTION_ERROR);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 0);

    proto = amxc_var_add_key(cstring_t, &settings, "proto", "pppoe");
    assert_int_equal(update_config(NULL, &settings, NULL), EXIT_OPTION_ERROR);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 0);

    amxc_var_set(cstring_t, proto, "none");
    set_default_settings(&settings, false, false);
    keepalive = amxc_var_add_key(cstring_t, &settings, "keepalive", "5 15");
    amxc_var_add_key(cstring_t, &settings, "auth_protocol", "PAP");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 0);

    // Enable
    amxc_var_set(cstring_t, proto, "pppoe");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 1);

    // Do Nothing, no new settings
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 1);

    // Update
    amxc_var_set(cstring_t, keepalive, "5 12");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 2);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    while(current_phase != PHASE_DEAD) {
        sleep(1);
    }
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 2);

    amount_of_link_starts = 0;
    amxc_var_clean(&settings);
}

void test_update_config_ipv6(UNUSED void** state) {
    amxc_var_t settings;
    amxc_var_t* proto = NULL;

    ipv6 = true;

    // Start
    amxc_var_init(&settings);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &settings, "proto", "pppoe");
    set_default_settings(&settings, true, true);
    amxc_var_add_key(bool, &settings, "ipv6", ipv6);
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 1);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    while(current_phase != PHASE_DEAD) {
        sleep(1);
    }
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 1);

    ipv6 = false;

    amount_of_link_starts = 0;
    amxc_var_clean(&settings);
}

void test_update_config_ipv6_setup_discovery_fails(UNUSED void** state) {
    amxc_var_t settings;
    amxc_var_t* proto = NULL;
    int link_starts = 0;

    ipv6 = true;
    discovery_fails = true;    // First time padi fails, process will restart.

    // Start
    amxc_var_init(&settings);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &settings, "proto", "pppoe");
    set_default_settings(&settings, true, true);
    amxc_var_add_key(bool, &settings, "ipv6", ipv6);

    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    link_starts = amount_of_link_starts;
    assert_true(link_starts > 1);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    while(current_phase != PHASE_DEAD) {
        sleep(1);
    }
    assert_int_equal(current_state, STATE_STOPPED);
    assert_true(amount_of_link_starts >= link_starts);

    discovery_fails = false;
    ipv6 = false;

    amount_of_link_starts = 0;
    amxc_var_clean(&settings);
}

void test_update_config_setup_authentication_fails(UNUSED void** state) {
    amxc_var_t settings;
    amxc_var_t* proto = NULL;
    int link_starts = 0;

    authenticate_fails = true; // Second time discovery succeeds but authentication fails, process will restart.

    // Start
    amxc_var_init(&settings);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &settings, "proto", "pppoe");
    set_default_settings(&settings, true, true);
    amxc_var_add_key(bool, &settings, "ipv6", ipv6);

    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    link_starts = amount_of_link_starts;
    assert_true(link_starts > 1);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    while(current_phase != PHASE_DEAD) {
        sleep(1);
    }
    assert_int_equal(current_state, STATE_STOPPED);
    assert_true(amount_of_link_starts >= link_starts);

    authenticate_fails = false;

    amount_of_link_starts = 0;
    amxc_var_clean(&settings);
}

void test_update_config_ipv6_setup_lcp_echo_fails(UNUSED void** state) {
    amxc_var_t settings;
    amxc_var_t* proto = NULL;
    int link_starts = 0;

    ipv6 = true;
    lcp_echo_fails = true;     // Third time connection is setup correctly but does not receive lcp echo replies, process will restart.

    // Start
    amxc_var_init(&settings);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &settings, "proto", "pppoe");
    set_default_settings(&settings, true, true);
    amxc_var_add_key(bool, &settings, "ipv6", ipv6);

    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    link_starts = amount_of_link_starts;
    assert_true(link_starts > 1);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    while(current_phase != PHASE_DEAD) {
        sleep(1);
    }
    assert_int_equal(current_state, STATE_STOPPED);
    assert_true(amount_of_link_starts >= link_starts);

    lcp_echo_fails = false;
    ipv6 = false;

    amount_of_link_starts = 0;
    amxc_var_clean(&settings);
}

void test_update_config_not_enable(UNUSED void** state) {
    amxc_var_t settings; // Third time connection is setup correctly but does not receive lcp echo replies, process will restart.

    // Start
    amxc_var_init(&settings);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &settings, "proto", "none");
    set_default_settings(&settings, true, true);
    amxc_var_add_key(bool, &settings, "ipv6", ipv6);

    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_UNCONFIGURED);
    assert_int_equal(amount_of_link_starts, 0);

    amxc_var_clean(&settings);
}

void test_phase_to_string(UNUSED void** state) {
    assert_string_equal(phase_to_string(PHASE_DEAD), "dead");
    assert_string_equal(phase_to_string(PHASE_INITIALIZE), "initialize");
    assert_string_equal(phase_to_string(PHASE_SERIALCONN), "serialconn");
    assert_string_equal(phase_to_string(PHASE_DORMANT), "dormant");
    assert_string_equal(phase_to_string(PHASE_ESTABLISH), "establish");
    assert_string_equal(phase_to_string(PHASE_AUTHENTICATE), "authenticate");
    assert_string_equal(phase_to_string(PHASE_CALLBACK), "callback");
    assert_string_equal(phase_to_string(PHASE_NETWORK), "network");
    assert_string_equal(phase_to_string(PHASE_RUNNING), "running");
    assert_string_equal(phase_to_string(PHASE_TERMINATE), "terminate");
    assert_string_equal(phase_to_string(PHASE_DISCONNECT), "disconnect");
    assert_string_equal(phase_to_string(PHASE_HOLDOFF), "holdoff");
    assert_string_equal(phase_to_string(PHASE_MASTER), "master");
    assert_null(phase_to_string(PHASE_MASTER + 1));
    assert_null(phase_to_string(-1));
    assert_null(phase_to_string(500));
}

void test_state_to_string(UNUSED void** state) {
    assert_string_equal(state_to_string(STATE_UNCONFIGURED), "unconfigured");
    assert_string_equal(state_to_string(STATE_STARTED), "started");
    assert_string_equal(state_to_string(STATE_STOPPING), "stopping");
    assert_string_equal(state_to_string(STATE_DIED), "died");
    assert_string_equal(state_to_string(STATE_WAIT_FOR_RESTART), "wait for restart");
    assert_string_equal(state_to_string(STATE_STOPPED), "stopped");
    assert_string_equal(state_to_string(STATE_RESET), "reset");
    assert_null(state_to_string(STATE_MAX + 1));
    assert_null(state_to_string(-1));
    assert_null(state_to_string(500));
}

void test_status_to_string(UNUSED void** state) {
    assert_string_equal(status_to_string(EXIT_OK), "ok");
    assert_string_equal(status_to_string(EXIT_FATAL_ERROR), "fatal error");
    assert_string_equal(status_to_string(EXIT_OPTION_ERROR), "option error");
    assert_string_equal(status_to_string(EXIT_NOT_ROOT), "not root");
    assert_string_equal(status_to_string(EXIT_NO_KERNEL_SUPPORT), "no kernel support");
    assert_string_equal(status_to_string(EXIT_USER_REQUEST), "user request");
    assert_string_equal(status_to_string(EXIT_LOCK_FAILED), "lock failed");
    assert_string_equal(status_to_string(EXIT_OPEN_FAILED), "open_failed");
    assert_string_equal(status_to_string(EXIT_CONNECT_FAILED), "connect failed");
    assert_string_equal(status_to_string(EXIT_PTYCMD_FAILED), "ptycmd failed");
    assert_string_equal(status_to_string(EXIT_NEGOTIATION_FAILED), "negotiation failed");
    assert_string_equal(status_to_string(EXIT_PEER_AUTH_FAILED), "peer authentication failed");
    assert_string_equal(status_to_string(EXIT_IDLE_TIMEOUT), "idle timeout");
    assert_string_equal(status_to_string(EXIT_CONNECT_TIME), "connect time");
    assert_string_equal(status_to_string(EXIT_CALLBACK), "callback");
    assert_string_equal(status_to_string(EXIT_PEER_DEAD), "peer dead");
    assert_string_equal(status_to_string(EXIT_HANGUP), "hangup");
    assert_string_equal(status_to_string(EXIT_LOOPBACK), "loopback");
    assert_string_equal(status_to_string(EXIT_INIT_FAILED), "init failed");
    assert_string_equal(status_to_string(EXIT_AUTH_TOPEER_FAILED), "authentication to peer failed");
    #ifdef MAXOCTETS
    assert_string_equal(status_to_string(EXIT_TRAFFIC_LIMIT), "traffic limit");
    #endif
    assert_string_equal(status_to_string(EXIT_CNID_AUTH_FAILED), "cnid authentication failed");
    assert_string_equal(status_to_string(EXIT_PEER_AUTH_NO_REPLY), "peer authentication no reply");
    assert_string_equal(status_to_string(EXIT_AUTH_TOPEER_NO_REPLY), "authentication to peer no reply");
    assert_null(status_to_string(EXIT_AUTH_TOPEER_NO_REPLY + 1));
    assert_null(status_to_string(-1));
    assert_null(status_to_string(500));
}

void test_get_timeout(UNUSED void** state) {
    char* str_value = NULL;

    str_value = strdup("10");
    assert_int_equal(get_timeout(str_value), 10);
    free(str_value);

    str_value = strdup("5,10,1000");
    for(int i = 0; i < 1000; i++) {
        int timeout = get_timeout(str_value);
        if((timeout < 5000) || (timeout > 10000) || (timeout % 1000 != 0)) {
            assert_true(false);
        }
    }
    free(str_value);

    str_value = strdup("0,120,100");
    for(int i = 0; i < 1000; i++) {
        int timeout = get_timeout(str_value);
        if((timeout < 0) || (timeout > 120000) || (timeout % 100 != 0)) {
            assert_true(false);
        }
    }
    free(str_value);

    str_value = strdup("0,50,10");
    for(int i = 0; i < 1000; i++) {
        int timeout = get_timeout(str_value);
        if((timeout < 0) || (timeout > 50000) || (timeout % 10 != 0)) {
            assert_true(false);
        }
    }
    free(str_value);

    str_value = strdup("0,3,1");
    for(int i = 0; i < 1000; i++) {
        int timeout = get_timeout(str_value);
        if((timeout < 0) || (timeout > 3000)) {
            assert_true(false);
        }
    }
    free(str_value);

    str_value = strdup("wrong");
    assert_int_equal(get_timeout(str_value), 0);
    free(str_value);

    str_value = strdup("10,120,100,10");
    assert_int_equal(get_timeout(str_value), 0);
    free(str_value);
}

void test_pppd_set_options(UNUSED void** state) {
    amxc_var_t settings;

    amxc_var_init(&settings);
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "proto", "pppoe");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "username", "softathome");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "password", "softathome");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "ac_name", "sah2r220");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "service_name", "");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "padi_interval", "1,2,4,8,16,32,64,128,-1");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "padr_interval", "5,5,5,5,5");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "lcp_conf_req_interval", "10000,10000,10000");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "lcp_term_interval", "5000,5000");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "ipcp_conf_req_interval", "10000,10000,10000");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "ipcp_term_req_interval", "5000,5000");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "auth_interval", "5000,5000");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "auth_failed_interval", "10000,-1");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "auth_protocol", "CHAP");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "ifname", "eth0");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "restart_delay", "5000");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "lcp_echo_restart_delay", "0,120,100");
    assert_int_equal(pppd_set_options(&settings), EXIT_OPTION_ERROR);

    amxc_var_add_key(cstring_t, &settings, "keepalive", "5 15");
    assert_int_equal(pppd_set_options(&settings), 0);

    amxc_var_clean(&settings);
}

void test_connection_stops(UNUSED void** state) {
    amxc_var_t settings;
    amxc_var_t* proto = NULL;

    // Start
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_init(&settings);

    amxc_var_set_type(&settings, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &settings, "proto", "pppoe");
    set_default_settings(&settings, true, true);
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 1);

    phase_change(NULL, PHASE_NETWORK);
    usleep(50);
    phase_change(NULL, PHASE_ESTABLISH);
    usleep(50);
    phase_change(NULL, PHASE_SERIALCONN);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &settings, NULL), 0);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 1);
    assert_int_equal(*(get_discovery_fd()), -1);
    assert_int_equal(ppp_fd, -1);
    assert_int_equal(ppp_dev_fd, -1);

    amount_of_link_starts = 0;
    amxc_var_clean(&settings);

}

void test_signals(UNUSED void** state) {
    assert_int_equal(amxp_sigmngr_emit_signal(NULL, strsignal(SIGHUP), NULL), 0);
    assert_int_equal(amxp_sigmngr_emit_signal(NULL, strsignal(SIGCHLD), NULL), 0);
    assert_int_equal(amxp_sigmngr_emit_signal(NULL, strsignal(SIGUSR1), NULL), 0);
    assert_int_equal(amxp_sigmngr_emit_signal(NULL, strsignal(SIGUSR2), NULL), 0);
}

/*
 * Test following edge case:
 * PPP connection is stopped for whatever reason and the restart timer is ticking before starting a new connection.
 * At this moment in time the user sets new PPP settings which means the connection needs to start again.
 */
void test_update_config_during_delayed_start(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t data;
    amxc_var_t* proto = NULL;
    amxc_var_t* auth_protocol = NULL;
    amxp_timer_cb_t* restart_cb = NULL;
    int compare_result = -1;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    // Start
    amxc_var_init(&data);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &data, "proto", "pppoe");
    auth_protocol = amxc_var_add_key(cstring_t, &data, "auth_protocol", "PAP");
    set_default_settings(&data, true, false);
    amxc_var_add_key(bool, &data, "ipv6", ipv6);
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 1);

    /*
     * At this point we have a normal connection running.
     * To emulate the situation described above we need to highjack the callback function of the restart timer.
     */
    restart_cb = &restart_timer->cb;
    restart_timer->cb = NULL;
    assert_true(restart_cb != NULL);

    // Update
    amxc_var_set(cstring_t, auth_protocol, "CHAP");
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_WAIT_FOR_RESTART);
    assert_int_equal(amount_of_link_starts, 1);

    assert_int_equal(amxc_var_compare(get_restart_settings(), &data, &compare_result), 0);
    assert_int_equal(compare_result, 0);

    // Manually call timer callback
    restart_timer_timeout(NULL, NULL);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 2);

    compare_result = -1;
    assert_int_equal(amxc_var_compare(get_current_settings(), &data, &compare_result), 0);
    assert_int_equal(compare_result, 0);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 2);

    restart_timer->cb = *restart_cb;

    amount_of_link_starts = 0;
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

/*
 * Test following edge case:
 * PPP connection is stopped for whatever reason and the restart timer is ticking before starting a new connection.
 * At this moment in time the user the user disables the PPP interface.
 */
void test_stop_during_delayed_start(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t data;
    amxc_var_t* proto = NULL;
    amxc_var_t* auth_protocol = NULL;
    amxp_timer_cb_t* restart_cb = NULL;
    int compare_result = -1;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    // Start
    amxc_var_init(&data);
    assert_int_equal(amount_of_link_starts, 0);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    proto = amxc_var_add_key(cstring_t, &data, "proto", "pppoe");
    auth_protocol = amxc_var_add_key(cstring_t, &data, "auth_protocol", "PAP");
    set_default_settings(&data, true, false);
    amxc_var_add_key(bool, &data, "ipv6", ipv6);
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_STARTED);
    assert_int_equal(amount_of_link_starts, 1);

    /*
     * At this point we have a normal connection running.
     * To emulate the situation described above we need to highjack the callback function of the restart timer.
     */
    restart_cb = &restart_timer->cb;
    restart_timer->cb = NULL;
    assert_true(restart_cb != NULL);

    // Update
    amxc_var_set(cstring_t, auth_protocol, "CHAP");
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_WAIT_FOR_RESTART);
    assert_int_equal(amount_of_link_starts, 1);

    assert_int_equal(amxc_var_compare(get_restart_settings(), &data, &compare_result), 0);
    assert_int_equal(compare_result, 0);

    // Stop
    amxc_var_set(cstring_t, proto, "none");
    assert_int_equal(update_config(NULL, &data, NULL), 0);
    assert_int_equal(current_state, STATE_STOPPED);
    assert_int_equal(amount_of_link_starts, 1);
    assert_null(get_current_settings());
    assert_null(get_restart_settings());

    restart_timer->cb = *restart_cb;

    amount_of_link_starts = 0;
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}
