MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/mod_coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(MOCK_SRCDIR)/*.c \
          $(wildcard $(SRCDIR)/*.c) \
		  )


CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -fPIC -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread -DUNIT_TEST \
		  -DINET6=1

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxb -lamxc -lamxd -lamxj -lamxm -lamxo -lamxp -ldl -lpthread

