/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "mock.h"
#include "mock_pppd.h"
#include <string.h>

#include <pppd/fsm.h>
#include <pppd/ipcp.h>
#include <pppd/ipv6cp.h>
#include "pppd_actions.h"
#include "pppd_callbacks.h"
#include "pppd_dm.h"
#include "pppd_signals.h"
#include "pppd_utils.h"
#include "ppp.h"

int asked_to_quit = 0;
int kill_link = 0;
int got_sighup = 0;
int got_sigterm = 0;
bool devnam_fixed = false;
volatile int status = EXIT_OK;

ipcp_options ipcp_hisoptions[];
ipcp_options ipcp_gotoptions[];
ipv6cp_options ipv6cp_hisoptions[];
ipv6cp_options ipv6cp_gotoptions[];

int ppp_fd = 0;
int ppp_dev_fd = 0;
int ppp_session_number = 0;
int fd_devnull = 0;

char* progname = NULL;
struct notifier* phasechange = NULL;

static void stop_connection(void) {
    phase_change(NULL, PHASE_TERMINATE);
    usleep(10);
    if(ipv6) {
        ipv6_down();
    } else {
        ip_down();
    }
    usleep(10);
    phase_change(NULL, PHASE_DISCONNECT);
    mru_changed(PPP_MRU);
    usleep(10);
    phase_change(NULL, PHASE_DEAD);
}

/*
 * Mocked channel functions
 *
 */

static int establish_ppp(int fd) {
    ppp_fd = fd;
    ppp_dev_fd = fd;

    return ppp_fd;
}

static void disestablish_ppp(UNUSED int dev_fd) {
    ppp_fd = -1;
    ppp_dev_fd = -1;
}

static void handle_discovery(void) {
    if(discovery_fails) {
        discovery_fails = false;
        phase_change(NULL, PHASE_DEAD);
        return;
    }

    the_channel->establish_ppp(20);

    status = EXIT_NEGOTIATION_FAILED;
    phase_change(NULL, PHASE_ESTABLISH);
    usleep(10);
    ppp_session_number = 42;
    mru_changed(PPP_MRU);
    discovery_succeeded();

    /* Instead of sending IPCP/LCP Conf request packets and waing for replies on the ppp_fd/ppp_dev_fd force this */
    ppp_get_input(NULL, NULL, NULL);
}

static void discovery(void) {
    /* Instead of sending PADI and waiting for PADO to come in on the discovery_fd force the discovery */
    ppp_handle_discovery(NULL, NULL, NULL);
}

struct channel pppoe_channel = {
    .options = NULL,
    .process_extra_options = NULL,
    .check_options = NULL,
    .connect = NULL,
    .disconnect = NULL,
    .establish_ppp = &establish_ppp,
    .disestablish_ppp = &disestablish_ppp,
    .send_config = NULL,
    .recv_config = NULL,
    .close = NULL,
    .cleanup = NULL,
    .discovery = &discovery,
    .handle_discovery = &handle_discovery
};

struct channel* the_channel = &pppoe_channel;

char* __wrap_ip_ntoa(UNUSED u_int32_t ipaddr) {
    return NULL;
}

char* __wrap_eui64_ntoa(UNUSED eui64_t ifaceid) {
    return NULL;
}

void __wrap_cleanup(void) {
    the_channel->disestablish_ppp(20);
    discovery_stopped();

    if(the_channel->cleanup) {
        (*the_channel->cleanup)();
    }
}

void __wrap_add_notifier(UNUSED struct notifier** notif, UNUSED notify_func func, UNUSED void* arg) {
}

int __wrap_options_from_list(UNUSED struct wordlist* w, UNUSED int priv) {
    return 1;
}

void __wrap_handle_events(UNUSED bool blocking) {
    kill_link = 0;

    if(got_sighup) {
        kill_link = 1;
        got_sighup = 0;
    }
    if(got_sigterm) {
        kill_link = 1;
        asked_to_quit = 1;
        status = EXIT_USER_REQUEST;
        got_sigterm = 0;
    }
}

void __wrap_get_input(void) {
    if(authenticate_fails) {
        authenticate_fails = false;
        status = EXIT_PEER_AUTH_FAILED;
        stop_connection();
    }
    phase_change(NULL, PHASE_AUTHENTICATE);
    mru_changed(1400);
    usleep(10);
    authentication_protocol("CHAP");
    usleep(10);
    phase_change(NULL, PHASE_NETWORK);
    usleep(10);
    encryption_protocol("MPPE");
    usleep(10);
    status = EXIT_OK;
    phase_change(NULL, PHASE_RUNNING);
    usleep(10);
    if(ipv6) {
        ipv6_up();
    } else {
        ip_up();
    }
    if(lcp_echo_fails) {
        lcp_echo_fails = false;
        status = EXIT_PEER_DEAD;
        stop_connection();
    }
}

void __wrap_calltimeout(void) {
}

void __wrap_hup(UNUSED int sig) {
    got_sighup = SIGHUP;
}

void __wrap_term(UNUSED int sig) {
    got_sigterm = SIGINT;
}

void __wrap_chld(UNUSED int sig) {
}

void __wrap_toggle_debug(UNUSED int sig) {
}

void __wrap_open_ccp(UNUSED int sig) {
}

int __wrap_init_pre_settings(UNUSED struct protent* protp) {
    asked_to_quit = 0;
    return 0;
}

int __wrap_init_settings(UNUSED struct protent* protp, UNUSED bool enable_signals) {
    phase_change(NULL, PHASE_INITIALIZE);
    return 0;
}

int __wrap_init_connection(UNUSED bool blocking) {
    return 0;
}

int __wrap_start_link (UNUSED int unit, UNUSED bool blocking) {
    amount_of_link_starts++;
    status = EXIT_CONNECT_FAILED;
    phase_change(NULL, PHASE_SERIALCONN);
    usleep(10);
    return 0;
}

int __wrap_cleanup_connection(UNUSED bool blocking) {
    return asked_to_quit;
}

int __wrap_cleanup_children(UNUSED bool blocking) {
    return 0;
}

int __wrap_react_on_handled_events(void) {
    if(kill_link) {
        if((current_phase != PHASE_DEAD) && (current_phase != PHASE_MASTER)) {
            stop_connection();
        }
        kill_link = 0;
    }
    return 0;
}

timepoints_t* __wrap_initialize_timepoints(UNUSED const char* timepoints) {
    timepoints_t* t = (timepoints_t*) calloc(1, sizeof(timepoints_t));
    return t;
}

int __wrap_next(UNUSED timepoints_t* t) {
    return 1000;
}

void __wrap_cleanup_timepoints(UNUSED timepoints_t** t) {
    free(*t);
    *t = NULL;
}

int __wrap_amxp_sigmngr_emit_signal(UNUSED const amxp_signal_mngr_t* const sig_mngr,
                                    const char* name,
                                    UNUSED const amxc_var_t* const data) {
    if(strcmp(name, strsignal(SIGHUP)) == 0) {
        pppd_hup(NULL, NULL, NULL);
    } else if(strcmp(name, strsignal(SIGINT)) == 0) {
        pppd_term(NULL, NULL, NULL);
    } else if(strcmp(name, strsignal(SIGCHLD)) == 0) {
        pppd_chld(NULL, NULL, NULL);
    } else if(strcmp(name, strsignal(SIGUSR1)) == 0) {
        pppd_usr1(NULL, NULL, NULL);
    } else if(strcmp(name, strsignal(SIGUSR2)) == 0) {
        pppd_usr2(NULL, NULL, NULL);
    } else if(strcmp(name, "ppp_died") == 0) {
        handle_died_ppp(NULL, NULL, NULL);
    }
    return 0;
}

int __wrap_amxp_timer_start(amxp_timer_t* timer,
                            UNUSED unsigned int timeout_msec) {
    if(timer->cb != NULL) {
        timer->cb(timer, NULL);
    }
    return 0;
}

int __wrap_sahTraceZoneLevel(UNUSED int tracezone) {
    return 100;
}

int __wrap_sahTraceGetZone(UNUSED const char* zone) {
    return 100;
}

void __wrap_sys_close(void) {

}